<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>

	<meta charset="utf-8">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/fa-svg-with-js.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/fontawesome-all.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

</head>
<body>

	<header>
		<nav class="navbar navbar-default">

			<div class="container">

				<div class="navbar-header">
					<a href="#" class="navbar-brand">Maxim Nyansa E-Assessment</a>
				</div>
				<ul class="nav navbar-right">
					<button class="btn btn-danger " type="button" data-toggle="modal" data-backdrop="static"
    	 data-keyboard="false" data-target="#signIn">Sign In</button>
				</ul>
			</div>
			
		</nav>
	</header>


<div class="container-fluid flevel img-responsive ">
	<div class="row justify-content-md-center">
	<div class="col-sm-6 " style="text-align: center; ">

		<div style="margin-top: 250px;">
		<div>
			<h1 class="p-3 mb-2 bg-gradient-danger text-white">Electronic Assessment Tool</h1>
		</div>
		<div>
			<h4 class="p-3 mb-2 bg-gradient-danger text-white">Create assessments with our easy, fun and user-friendly online assessment software! </h4>
		</div>
		<div>
			<button class="btn btn-danger " type="button" data-toggle="modal" data-target="#signIn">Sign In</button>
			<button class="btn btn-info " type="button" data-toggle="modal" data-target="#register">Create an Account</button>
		</div>
		</div>

	</div>
	</div>
</div>


<!-- FEATURES DESCRIPTION DIV -->
<div class="jumbotron" style="margin-top: 0px;">
	<div class="row justify-content-md-center" style="text-align: center;">
		<div class="col-sm-3 offset-1">
			<div class = "">
    			<span class="fas  fa-tv fa-spin big-icon"></span>
    			<div style="margin-top: 20px; ">
    				<h3>Easy to use</h3>
    				<p>Our online assessment tool is simple <br>and easy to use. For participants as<br> well as for the administrators. With our online assessment software it's<br> easy to start, which makes it more<br>fun</p>
    			</div>
			</div>
		</div>
		<div class="col-sm-3 offset-0">
			<div class = "">
    			<span class="fas fa-cog fa-spin big-icon"></span>
    			<div style="margin-top: 20px;">
    				<h3>Looks great on all devices</h3>
    				<p>It doesn't matter if you are on a <br>phone, table or pc: your online<br>assessment will look beautiful. Our<br> assessment tool is fully responsive, so<br> you can engage your audience on any<br>platform</p>
    			</div>

			</div>
		</div>
		<div class="col-sm-3 offset-0">
			<div class = "">
    			<span class="fas  fa-balance-scale fa-spin big-icon"></span>
    			<div style="margin-top: 20px;">
    				<h3>Awesome support</h3>
    				<p>Do you have any questions about our<br> online assessment tool? Our support<br> owls are always by your side and provide you with the best possible support!</p>
    			</div>
			</div>
		</div>
	</div>
</div>

 <!-- ACCOUNT DIV -->


<div class="container-fluid" style="margin-top: 0px;">
	<div class="row justify-content-md-center" style="text-align: center;">
		<div class="col-sm-6 ">
			<div class = "">
    			
    			<div class="text-details">
    				<h1>STEP 1: CREATE ACCOUNT</h1>
						<p>Create an account, confirm account creation via email, sign in to start creating assessments</p>
    			</div>
			</div>
		</div>
		<div class="col-sm-6 ">
			<div class = "">
    			
    			<div class="alevel">
    				<!-- <img src="img/cor.jpg" class="img-responsive"> -->
    			</div>

			</div>
		</div>
		
	</div>
</div>



<div class="container-fluid" style="margin-top: 0px;">
	<div class="row justify-content-md-center" style="text-align: center;">
		<div class="col-sm-6 ">
			<div class = "">
    			
    			<div class="blevel">
    				<!-- <img src="img/cor.jpg" class="img-responsive" > -->
    			</div>
			</div>
		</div>
		<div class="col-sm-6 ">
			<div class = "">
    			
    			<div class="text-details" >
    				<h1>STEP 2: CREATE ASSESSMENT</h1>
						<p>Click on assessments, create new assessment, Choose the assessment type, fill in the required information and submit assessment</p>
    			</div>

			</div>
		</div>
		
	</div>
</div>


<!-- manage assessment -->
<div class="container-fluid" style="margin-top: 0px;">
	<div class="row justify-content-md-center" style="text-align: center;">
		<div class="col-sm-6 ">
			<div class = "">
    			
    			<div class="text-details">
    				<h1>STEP 3: MANAGE ASSESSMENT</h1>
						<p>See all assessment, view all details, delete assessment, edit assessment, Logout etc</p>
    			</div>
			</div>
		</div>
		<div class="col-sm-6 ">
			<div class = "justify-content-md-center">
    			
    			<div class="clevel">
    				
    			</div>

			</div>
		</div>
		
	</div>
</div>



<div class="container-fluid llevel img-responsive ">
	<div class="row justify-content-md-center">
		<div class="" style="">
		<form class="form-inline" row="form" >
			<h2 class="p-3 mb-2 bg-gradient-danger text-white">Want to recieve update notifications? Opt is here!
			</h2><br>
				<div class="form-group" >
    				<input type="email" class="form-control input-lg" id="email" placeholder="Enter your Email" >
    					<button  type="submit" class="btn btn-primary" name="">Opt in!</button>
  				</div>
		</form>
		</div>
	</div>
</div>


<!-- my footer -->
	<footer>
		<div class="">
		<ul class="nav footer-height form-inline">
  			<li class="nav-item">
    			<a class="nav-link active" href="#">About Us</a>
  			</li>
  			<li class="nav-item">
    			<a class="nav-link" href="#">Contact</a>
  			</li>
  			<li class="nav-item">
    			<a class="nav-link" href="#">Term Of Use</a>
  			</li>
  			<li class="nav-item">
    			<a class="nav-link" href="#">Privacy Policy</a>
  			</li><br>
  			<li>
  				<span class="text-muted justify-content-end">@jiro</span>
  			</li>

		</ul>


		</div>
	</footer>


<!-- modal for sign in -->
<div class="modal" id="signIn">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" class="text-muted">Please sign In</h4>
				<button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<div>
					<div class="form-group">
						<label for="email" class="text-muted">E-mail: </label>
						<input type="email" name="email" placeholder="eg. example@gmail.com" class="form-control">
					</div>
					<div class="form-group">
						<label for="password" class="text-muted">Password: </label>
						<input type="password" name="password" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" type="button">Submit</button>
			</div>
		</div>
	</div>
	
</div>


<!-- modal for registeration -->
<div class="modal" id="register">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" class="text-muted">Please Register with Us</h4>
				<button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<div>
					<div class="form-group">
						<label for="fname" class="text-muted">First Name: </label>
						<input type="text" name="fname" placeholder="Your First Name" class="form-control">
					</div>
					<div class="form-group">
						<label for="othername" class="text-muted">Other Name: </label>
						<input type="text" name="othername" placeholder="Your Other name" class="form-control">
					</div>
					<div class="form-group">
						<label for="email" class="text-muted">E-mail: </label>
						<input type="email" name="email" placeholder="eg. example@gmail.com" class="form-control">
					</div>
					<div class="form-group">
						<label for="password" class="text-muted">Password: </label>
						<input type="password" name="password" class="form-control">
					</div>
					<div class="form-group">
						<label for="password" class="text-muted">Confirm Password: </label>
						<input type="password" name="password" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-warning" type="button">Submit</button>
			</div>
		</div>
	</div>
	
</div>

</body>
</html>